#!/usr/bin/env python3
import tkinter as tk
import serial
import os
import time
import csv
import threading
from tkinter import ttk
from tkinter import messagebox

dirname = os.path.dirname(__file__)

#    BORRAR ÚLTIMA LINEA DE HISTORIAL.csv
#    sed -i '$ d' historial.csv

BAUDIOS = 600
#BAUDIOS = 1200

flag_impresion=True
LECTURABALANZA="No senal"
TEMPORAL=0

def lecturaBalanza():
    contador=0
    try:
        global LECTURABALANZA
        global BAUDIOS
        global TEMPORAL
        ser = serial.Serial(
        port='/dev/ttyUSB0',
        baudrate=BAUDIOS,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0)
        linea = ""
        flag = 0
        flag2 = 0
        cuentaNoLeidas = 0

        while True:
            for caracterLeido in ser.read():

                if caracterLeido == 0x0d:

                    if flag == 1:
                        if linea != "":
                            linea2 = linea
                            # print("No vacia")
                            cuentaNoLeidas = 0
                            LECTURABALANZA = linea
                            linea=""
                        else:
                            if cuentaNoLeidas > 10:
                                return ("Sin senal")
                            cuentaNoLeidas += 1
                            LECTURABALANZA = linea2
                    flag = 1

                if flag == 1:
                    if flag2 == 1:

                        if ((chr(caracterLeido) <= "9" and chr(caracterLeido) >= "0") or (chr(caracterLeido) == ".") or (chr(caracterLeido) == ",")):
                            linea = linea+chr(caracterLeido)
                        # linea = linea+chr(caracterLeido)

                    flag2 = 1
        ser.close()

    except Exception:
        TEMPORAL+=1
        time.sleep(1)
        try:
            ser.close()
        except Exception:
            pass
        lecturaBalanza()
        pass
        #print("Lectura fallida")


def refresher():
    global LECTURABALANZA
    global TEMPORAL
    #global text
    text.configure(text=time.asctime())
    #peso_label.configure(text=lecturaBalanza())
    peso_label.configure(text=LECTURABALANZA)
    # print(LECTURABALANZA)
    # print("TEMPORAL: "+str(TEMPORAL))
    root.after(100, refresher)


def disable_event():
    pass


def cerrar_clicked():
    print("Cerrado")
    return(newWindow.destroy())

def comprueba_blancos(variables):
    for j in range(len(variables)):
        if not(variables[j]):
            return False
    return True

def buscaCodigos(archivo,valor):
    global flag_impresion
    #flag_impresion=True
    #archivo = input("Archivo a buscar: ")
    #print("Archivo: "+archivo+"\nValor: "+valor)
    try:
        csv_file = csv.reader(open('campos_validos/'+archivo,"r"),delimiter=",")
        #numero = input("Número a buscar: ")

        for row in csv_file:
            if valor == row [0]:
                print("ENCONTRADO: "+row[1])
                print("Valor Flag: "+str(flag_impresion))
                #return "Encontre"
                return row[1]

        print("Valor No Hallado")
        print("Valor Flag: "+str(flag_impresion))
        flag_impresion=False
        tk.messagebox.showwarning('Advertencia', archivo+' no encontrado')
        return ""
    except Exception:
        print ("No encontró Archivo")
        return ""

def impr_fijos(etiqueta, renglon, ult_renglon, variables,peso,nombreventana):
    global pesoCuerpo
    # print(peso_label)
    # print("Peso: ",(peso_label.cget("text")+" KG."))
    result = time.localtime()
    fecha = str(result.tm_mday)+"/"+str(result.tm_mon)+"/"+str(result.tm_year)
    hora = str(result.tm_hour)+":"+str(result.tm_min)+":"+str(result.tm_sec)
#     datos= ("Fecha: "+fecha,
#             "Hora: "+hora,
#             #"Peso: "+peso_label.get("text")+" KG."
#             )
    pesoTexto = ("Peso: "+peso+" KG.")
    if (not(pesoCuerpo.get()) and nombreventana!="Scrap"):pesoTexto=""
    if nombreventana=="Bultos":
        pesoTexto=""
    # print("PEsotexto"+pesoTexto )
    # print (nombreventana)
    # print("PEPEPE")
    # if len(variables) < 7:
    datos = ("Fecha: "+fecha,
             "Hora: "+hora,
             # "Peso: "+pesoTexto
             pesoTexto
             )
    #datos = ("Fecha: "+fecha,
             #"Hora: "+hora,
             #"Peso: "+peso_label.cget("text")+" KG."
             #)
    # else:
        # datos = ("Fecha: "+fecha,
                 # "Hora: "+hora,
                 # "Peso: "+peso_label.cget("text")+" KG."
                 # )
#     if len(variables)<7:
#         datos= ("Fecha: "+fecha,
#                 "Hora: "+hora,
#                 "Peso: "+peso_label.cget("text")+" KG."
#                 )
#     else:
#         datos= ("Fecha: "+fecha,
#                 "Hora: "+hora,
#                 "Peso: "+peso_label.cget("text")+" KG."
#                 )
    for j in range(len(datos)):
        # print ("JOTA ="+str(j))
        # etiqueta.write("^FO30,"+str(i*40)+"^ADN,36,20^FD"+j[0]+": "+j[1]+"^FS\n")
        # print("^FO30,"+str((j+1)*renglon)+"^ADN,36,20^FD"+datos[j]+"^FS\n")
        etiqueta.write("^FO30,"+str((j+1+ult_renglon)*renglon)+"^ADN,36,20^FD"+datos[j]+"^FS\n")
    return j


def impr_variables(etiqueta, nombres, variables, renglon, ult_renglon):

    global pesoCuerpo
    global flag_impresion
    flag_impresion=True

    for j in range(len(nombres)):

        #descripcionCodigo=buscaCodigos(nombres[j],variables[j])
        variables[j]+=(" "+buscaCodigos(nombres[j],variables[j]))

        #etiqueta.write("^FO30,"+str((j+1+ult_renglon)*renglon)+"^ADN,36,20^FD"+nombres[j]+": "+variables[j]+" - "+ descripcionCodigo+"^FS\n")
        etiqueta.write("^FO30,"+str((j+1+ult_renglon)*renglon)+"^ADN,36,20^FD"+nombres[j]+": "+variables[j]+"^FS\n")
    # return j


def aniadir_logo(etiqueta, x, y):
    # x=700
    # y=30
    etiqueta.write ("""^FO"""+str(x)+","+str(y)+"""^GFA,512,512,8,L03FFC,K03JFC,J01LF8,J07LFE,J0FF8003FF8,I03FCJ07FC,I07FK01FF,001FCL07F8,003F8L03FC,007E003F8001FE,00FC01IF800FF,01F807JF007F8,01F01KFC03FC,03E03KFE01FC,07E07LF81FE,07C0MFC0FF,0F80FCI07FF0FF,1F81F8J0FF87F81F81EK03FC7F83F03CL0FE3FC3F03CL03F3FC3F078L01FBFC7F078M0IFE7F078M07FFE7E078M03FFE7E078M01!FF078I038I0!FF03C007FF8007!FF03E07JF003!FF03MF803!FF01MFC01!FF01MFE01!FF80NF01!FF807MF80!FF803MF80!FFC00MFC0!FFC003LFC07!FFEI07KFC07!7FEM03FC07!7FFM01FE07E7FF8M0FE07E7FFCM0FC0FE3FFEM0FC0FC3IFM0FC0FC3IF8L0FC0FC1IFEL0F80F81JFK01F81F80FFBFCJ03F01F,0FF9FFJ0FF03F,07F87FF003FE03E,03FC3LFC07C,03FC0LF80FC,01FE03KF01F8,00FF00JFC01F,007F801IF003E,003FC001FI0FC,001FFL01F8,I0FFCK07F,I03FFJ01FC,I01FFEI07F8,J07LFE,J01LF8,K03JFC,L07FFE,^FS\n""")


def aniadir_qr(etiqueta, x, y, escala, variables, peso):
    result = time.localtime()
    fecha = str(result.tm_mday)+"/"+str(result.tm_mon)+"/"+str(result.tm_year)
    hora = str(result.tm_hour)+":"+str(result.tm_min)+":"+str(result.tm_sec)
    # registro=(fecha+";"+hora)

    texto = ""
    texto = (fecha+";"+hora+";"+peso+";"+variables[0])

    for j in range(len(variables)-1):
        texto = texto+";"+(variables[j+1])

    etiqueta.write("""^FO"""+str(x)+","+str(y)+"""
                    ^BQN,2,"""+str(escala)+"""
                    ^FDHA,"""+texto+"""
                    ^FS\n""")


def guardar_registro(variablestemp, peso, fecha, hora, nombreventana,nombrestemp):
    print("Intentando guardar")
    nombres=nombrestemp+["ComputadoraOrigen","NombreVentana","Fecha","Hora","Peso"]
    variables=variablestemp+[obtieneid(),nombreventana,fecha,hora,peso]
    # result = time.localtime()
    # fecha= str(result.tm_mday)+"/"+str(result.tm_mon)+"/"+str(result.tm_year)
    # hora= str(result.tm_hour)+":"+str(result.tm_min)+":"+str(result.tm_sec)
    # tempi=["Computadora"],["Computadora2"],["Computadora3"]
    # a={}
    # a.append("Computadora1")
    # a.append("Computadora2")
    # a.append("Computadora3")
    # a.append("Computadora4")
    nombres.append("ComputadoraOrigen")
    nombres.append("NombreVentana")
    nombres.append("Fecha")
    nombres.append("Hora")
    nombres.append("Peso")
    variables.append(obtieneid())
    variables.append(nombreventana)
    variables.append(fecha)
    variables.append(hora)
    variables.append(peso)

    # nombres.append(["Maquina","Proceso","Fecha","Hora","Peso"])
    # variables.append(obtieneid()+";"+nombreventana+";"+fecha+";"+hora+";"+peso)
    # registro = (obtieneid()+";"+nombreventana+";"+fecha+";"+hora+";"+peso)
    registro = ""

    # for j in range(0, len(variables)):
    #     print("Sasa: "+nombres[j]+" : "+variables[j])
    #     registro = (registro+";"+variables[j])
    # print("Registro: "+registro)

    # field_names = nombres
    with open('/home/balanza/Documentos/SoftwareBalanza/historial/nuevoHistorial.csv') as f_object:
        csv_reader = csv.reader(f_object, delimiter = ';')
        field_names = []
        for row in csv_reader:
            for j in row:
                field_names.append(j)
            break
    print("Columnas: ",field_names)

    # field_names = ['Hora', 'Cliente', 'NombreVentana', 'Trabajo', 'Ot', 'Codigo', 'Maquina', 'Operario', 'ComputadoraOrigen', 'Fecha','Peso','Otro','Otro2']

    # dict = {'ComputadoraOrigen': 6, 'Fecha': 'c', 'Hora': 5532}

    dict = {}

    for j in range(0, len(variables)):
        dict[nombres[j]]=variables[j]
        # print("Sasa: "+nombres[j]+" : "+variables[j])
        # registro = (registro+";"+variables[j])
    # print("Registro: "+registro)
    print("Diccionario: "+str(dict))
    print("Nombres aa: ",nombres)

    # Open CSV file in append mode
    # Create a file object for this file
    with open('/home/balanza/Documentos/SoftwareBalanza/historial/nuevoHistorial.csv', 'a') as f_object:

        # Pass the file object and a list
        # of column names to DictWriter()
        # You will get a object of DictWriter
        dictwriter_object = csv.DictWriter(f_object, fieldnames=field_names,delimiter=";")

        # Pass the dictionary as an argument to the Writerow()
        dictwriter_object.writerow(dict)

        # Close the file object
        f_object.close()

    #
    # with open('/historial/'+nombreventana+'.csv', 'a') as f:
    #         f.write(registro+"\n")



    # try:
    #     with open('/home/balanza/Documentos/SoftwareBalanza/historial/'+nombreventana+'.csv', 'a') as f:
    #         f.write(registro+"\n")
    # except Exception:
    #     with open('historial/'+nombreventana+'.csv', 'a') as f:
    #         f.write(registro+"\n")


def imp_troquel(etiqueta, nombres, variables, renglon, ult_renglon, peso):
    renglon = 25
    posicionX = "30"
    cantRenglones=8
    aux=ult_renglon
    # try:
    #     for j in range(len(nombres)):
    #         print("pa")
    #         etiqueta.write("^FO"+posicionX+","+str((j+1+ult_renglon)*renglon)+"^ADN,1,1^FD"+nombres[j]+": "+variables[j]+"^FS\n")
    #         print("J Value: "+str(j))
    #         if j>8:
    #             if posicionX != "430":
    #                 pass
    #             posicionX = "430"
    # except Exception:
    #     for j in range(len(variables)):
    #         print("pe")
    #         etiqueta.write("^FO30,"+str((j+1+ult_renglon)*renglon)+"^ADN,1,1^FD"+nombres[j]+": "+variables[j]+"^FS\n")
    # j += 1
    # if j>8:
    #     posicionX = "430"

    result = time.localtime()
    fecha = str(result.tm_mday)+"/"+str(result.tm_mon)+"/"+str(result.tm_year)
    hora = str(result.tm_hour)+":"+str(result.tm_min)+":"+str(result.tm_sec)
    datos =[fecha+" "+hora,
             # "Hora: "+hora,
             "Peso: "+peso+" KG."
             ]
#     datos= ("Fecha: "+fecha,
#             "Hora: "+hora,
#             "Peso: "+peso_label.cget("text")+" KG."
#             )
    for j in range(len(nombres)):
        datos.append(nombres[j]+": "+variables[j])
        
    i=0
    for p in range(len(datos)):
        # print("pi")
        etiqueta.write("^FO"+posicionX+","+str((i+1+ult_renglon)*renglon)+"^ADN,1,1^FD"+datos[p]+"^FS\n")
        i=i+1
        if p>cantRenglones:
            if posicionX != "430":
                ult_renglon=aux
                i=0
            posicionX = "430"
    
    
    
    j= 8
    return j


def vaciar_clicked(entries):
    j = 0
    for j in range(len(entries)):
        entries[j].delete(0, 'end')
    print("VaciaR")


def imprimir_clicked(nombres, variables_bruto,nombreventana):

    global flag_impresion
    global LECTURABALANZA
    global diceSuperbol
    global logoCuerpo

    variables=[]

    for p in range((len(variables_bruto))):
        print ("LETRA PE: "+str((p)))
        variables.append(variables_bruto[p].get())
    # variables.append(nombreventana)

    #variables=variables_bruto
    diccionarioTemporal={}

    for x in range(len(nombres)):
        print("Posición: "+ str(x))
        print("Nombre: "+nombres[x])
        print("Variable:"+variables[x])
        diccionarioTemporal[nombres[x]]=variables[x]

    diccionarioTemporal["Maquina"]={"Parabailara":"Pepe","Variable2":"Carlitos"}

    print("DICCIONARIO: "+str(diccionarioTemporal["Maquina"]))
    print("DICCIONARIO ANIDADO"+diccionarioTemporal["Maquina"]["Variable2"])

    print("Nombres: ")
    print(nombres)

    print("Variables_bruto: ")
    print(variables_bruto)

    print("Variables: ")
    print(variables)

    print("Nombre ventana: ")
    print(nombreventana)

    print("DICCIONARIO: ")
    print(diccionarioTemporal)


    if comprueba_blancos(variables):

        tam_renglon = 40
        ult_renglon = 0

        peso = LECTURABALANZA

        try:
            etiqueta = open('/home/balanza/Documentos/SoftwareBalanza/prueba.zpl', 'w')
        except Exception:
            etiqueta = open('prueba.zpl', 'w')

        etiqueta.write("^XA\n")
        etiqueta.write("^FO530,275^ADN,1,1^FD"+identificador+"^FS\n")
        etiqueta.write("^FO530,1400^ADN,1,1^FD"+identificador+"^FS\n")
        
        if diceSuperbol.get(): etiqueta.write("^FO30,1400^ADN,36,20^FDSUPERBOL S.R.L.^FS\n")
        if logoCuerpo.get(): aniadir_logo(etiqueta, 700, 350)


        ult_renglon=imp_troquel(etiqueta, nombres, variables, tam_renglon, ult_renglon, peso)

        # ult_renglon+=3

        ult_renglon += impr_fijos(etiqueta, tam_renglon, ult_renglon, variables,peso,nombreventana) # Agrega valores fijos a etiquetas
        ult_renglon += 1
        impr_variables(etiqueta, nombres, variables, tam_renglon, ult_renglon) #

        # aniadir_logo(etiqueta, 700, 30)
        # aniadir_logo(etiqueta, 700, 350)
        if nombreventana=="Scrap":
            aniadir_qr(etiqueta, 550, 90, 2, variables, peso)
        if nombreventana!="Impresion":
            aniadir_qr(etiqueta, 200, 1100, 4, variables, peso)

        etiqueta.write("^XZ")
        # Agrega valores variables a etiquetas
        # impr_limita_zpl() #Abre, pone al principio y al final del archivo las cosas para abrir y cerrar archivos ZPL
        etiqueta.close()

        #pesoaguardar = peso_label['text']

        result = time.localtime()
        fecha = str(result.tm_mday)+"/"+str(result.tm_mon)+"/"+str(result.tm_year)
        hora = str(result.tm_hour)+":"+str(result.tm_min)+":"+str(result.tm_sec)

        if (flag_impresion):
            estado_impresion = os.popen('lpstat').read()
            if (estado_impresion == ""):

                try:
                    os.system('lp /home/balanza/Documentos/SoftwareBalanza/prueba.zpl')
                except Exception:
                    os.system('lp prueba.zpl')
                print("Se mandó")

                tk.messagebox.showinfo('Impresion en curso', 'Impresion en curso')
                # time.sleep(4)
                cont_intentos = 0
                while os.popen('lpstat').read() != "":
                    cont_intentos += 1
                    time.sleep(0.5)
                    if cont_intentos == 40:
                        tk.messagebox.showinfo('information', 'Problema de impresion')
                        return 0

                tk.messagebox.showinfo('information', 'Impresion finalizada')

                estado_impresion = os.popen('lpstat').read()
                # time.sleep(7)
                print("Estado")
                print(estado_impresion)
                print("Impresion")
                print("FLAG IMPRESION:  "+str(flag_impresion))
                # time.sleep(1)
                # guardar_registro(variables) ### TEMPORAL
                if (estado_impresion == ""):
                    print("Registro guardado")
                    guardar_registro(variables, peso, fecha, hora, nombreventana,nombres)
                else:
                    tk.messagebox.showinfo('information', 'Otra impresion en curso, volver a intentar')
                    # print ("problema de impresion")
                    # os.system('lprm -')

                    # Guardo Registro

            else:
                tk.messagebox.showinfo('information', 'Error de impresion')
                print("Existe otra impresión en curso")
                os.system('lprm -')
        else:
            pass
            #tk.messagebox.showwarning('Advertencia', 'Codigo no encontrado')

    else:
        tk.messagebox.showwarning('Advertencia', 'Completar todos los campos')


def seleccion_clicked(variable):
    print("Seleccion")
    print(variable)


def nuevaventana():
    def cerrar_clicked():
        print("Cerrado")
        return(newWindow.destroy())

    def dibujar():
        for x in pestanias:
            print(x)
        try:
            with open('/home/balanza/Documentos/SoftwareBalanza/pestanias/'+pestania_sel, newline='') as csvfile:
                data = list(csv.reader(csvfile))
                print(data)
        except Exception:
            with open('pestanias/'+pestania_sel, newline='') as csvfile:
                data = list(csv.reader(csvfile))
                print(data)

        x = data

        temp = 0
        variables_bruto = []
        entries = []
        nombre_va = []


        
        Campos = tk.Frame(newWindow)
        Botones = tk.Frame(newWindow)

        Campos.grid(column=1,row=1)
        Botones.grid(column=1,row=2)
                
        j=0
        
        for i in x:
            j=j+1
            if (i[2] == "texto"):
                va = tk.StringVar()
                campo = ttk.Label(Campos, text=i[0])
                campo.grid(column=1, row=j)
                en = ttk.Entry(Campos, textvariable=va)
                en.grid(column=2,row=j)
                variables_bruto.append(va)
                entries.append(en)
                nombre_va.append(i[0])

            elif (i[2] == "radio"):
                radios = i[0].split("#")
                print(radios)

                va = tk.StringVar()
                campo = ttk.Label(Campos, text=radios[0])
                campo.grid(column=1,row=j)
                radios_frame = ttk.Frame(Campos)
                radios_frame.grid(column=2,row=j)

                for size in range(1, len(radios)):
                    print(size)
                    radio = ttk.Radiobutton(
                        radios_frame,
                        text=radios[size],
                        value=radios[size],
                        variable=va
                    )
                    radio.pack(side='left', fill='both', expand=True)
                variables_bruto.append(va)
                entries.append(en)
                nombre_va.append(radios[0])

        # guardar_button = ttk.Button(newWindow, text="Guardar", command=lambda:guardar_clicked(nombre_va,variables)).pack(side='bottom',fill='x', expand=True, pady=10)
        # temporal_button = ttk.Button(newWindow, text="Temporal", command=lambda:validar(variables,peso_label,entries)).pack(side='bottom',fill='x', expand=True, pady=10)
        # pruebas_button = ttk.Button(newWindow, text="Prueba Guardar", command=lambda:pruebas_clicked(nombre_va,variables)).pack(side='bottom',fill='x', expand=True, pady=10)
        nombreventana=newWindow.title()
        imprimir_button = ttk.Button(Botones, text="Imprimir", command=lambda: imprimir_clicked(nombre_va, variables_bruto,nombreventana))
        # cerrar_button = ttk.Button(newWindow, text="Cerrar", command=cerrar_clicked)
        imprimir_button.grid(column=1,row=1)
        # vaciar_button = ttk.Button(newWindow, text="Vaciar campos", command=lambda: vaciar_clicked(entries)).pack(side='bottom', fill='x', expand=True, pady=10)

    newWindow = tk.Toplevel(root)
    newWindow.title(pestania_sel)
    newWindow.iconphoto(False, p1)

    dibujar()


def nuevaventana_clicked():
    nuevaventana()


def botones_pestanias():
    seleccion_button = {}
    for p in pestanias:
        def func(x=p):
            return (entry_update(x))

        seleccion_button[p] = ttk.Button(root, text=p, command=func).pack(side='left', fill='x', expand=True, pady=10)


def cerrar_clicked():
    print("Cerrado")
    return(newWindow.destroy())


def entry_update(variable):
    global pestania_sel
    pestania_sel = variable

    nuevaventana()

    return(variable)


def create_window():
    window = tk.Toplevel(root)

def obtieneid():
    try:
            with open('/home/balanza/Documentos/identificador', 'r') as archivo:
                temp = archivo.read()
                temp = temp.rstrip()
    except Exception:
            temp="NoId"
    return temp


threading.Thread(target=lecturaBalanza, daemon=True).start()

identificador=obtieneid()

root = tk.Tk()
root.title('Multi con poco')

try:
    p1 = tk.PhotoImage(file='/home/balanza/Documentos/SoftwareBalanza/LOGO.png')
except Exception:
    p1 = tk.PhotoImage(file='LOGO.png')


root.iconphoto(False, p1)

pestania_sel = ""

try:
    pestanias = os.listdir("/home/balanza/Documentos/SoftwareBalanza/pestanias")
except Exception:
    pestanias = os.listdir("pestanias")

tempi = 0

peso_label = ttk.Label(root, text="No hay senal")
peso_label.pack(fill='x', expand=True)
peso_label.config(font=("Courier", 44), anchor="center")

text = tk.Label(root, text='Error de hora')
text.pack(fill='x', expand=True)


botones_pestanias()

root.title('Sistema Pesaje Superbol')
root.protocol("WM_DELETE_WINDOW", disable_event)

diceSuperbol = tk.IntVar()
logoCuerpo = tk.IntVar()
pesoCuerpo = tk.IntVar()

#pesoCuerpo.set(1)

c1 = tk.Checkbutton(root, text='Colocar Logo',variable=logoCuerpo, onvalue=1, offvalue=0)
# c1.grid(column=1,row=12)
c1.pack()
c2 = tk.Checkbutton(root, text='Colocar Peso',variable=pesoCuerpo, onvalue=1, offvalue=0)
# c2.grid(column=1,row=13)
c2.pack()
c3 = tk.Checkbutton(root, text='Aclara texto Superbol',variable=diceSuperbol, onvalue=1, offvalue=0)
# c3.grid(column=1,row=14)
c3.pack()

refresher()

root.mainloop()
